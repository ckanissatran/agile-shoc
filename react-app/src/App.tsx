import React from 'react';
import {BrowserRouter, Route, RouteComponentProps, Switch} from "react-router-dom";
import {Retro} from "./retro/retro";

export const App: React.FC<RouteComponentProps> = () => {
  return (
      <BrowserRouter>
        <Switch>
            <Route exact path="/" component={Retro}/>
            <Route exact path="/retro" component={Retro}/>
        </Switch>
      </BrowserRouter>
  )};

export default App;
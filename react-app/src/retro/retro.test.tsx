import * as React from "react";
import {shallow, ShallowWrapper} from "enzyme";
import {Retro} from "./retro";
import {RetroTable} from "./retro-table";
import {NiceButton} from "../form/nice-button";

describe('retro', () => {
    let subject: ShallowWrapper;

    beforeEach(() => {
        subject = shallow(<Retro/>);
    });

    it('should show the word retro on the retro page', () => {
        expect(subject.contains("Retro")).toEqual(true);
    });

    it('should render a RetroTable component', () => {
        expect(subject.find(RetroTable)).toEqual(true);
    });

    it('should have a link to ActionItemModal', () => {
        expect(subject.find(NiceButton)).toEqual(true);
    });

});
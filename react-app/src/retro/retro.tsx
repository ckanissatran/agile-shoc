import * as React from "react";
import {RetroTable} from "./retro-table";
import {NiceButton} from "../form/nice-button";
import styles from "./retro.module.scss"

export const Retro = () => {


    return <div className={styles.background}>
        <div className={styles.flexIt}>
            <h1>Retrospective</h1>
            <NiceButton link={"/"} buttonText={"yo"}/>
        </div>
        <RetroTable/>
    </div>;
};
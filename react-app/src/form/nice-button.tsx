import * as React from "react";

interface ButtonProps {
    buttonText: string;
    link: string;
}

export const NiceButton: React.FC<ButtonProps> = ({buttonText, link}) => {
    return <a href={link}>{buttonText}</a>;
};
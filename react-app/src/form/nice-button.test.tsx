import * as React from "react";
import {shallow, ShallowWrapper} from "enzyme";
import {NiceButton} from "./nice-button";

describe('nice-button.tsx', () => {
    let subject: ShallowWrapper;

    beforeEach(() => {
       subject = shallow(<NiceButton buttonText={"Potato"} link={"/potato"}/>)
    });

    it('should show its buttonText', () => {
        expect(subject.contains("Potato")).toEqual(true);
    });
});